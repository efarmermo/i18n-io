name := "i18n-io"

organization := "mobi.efarmer.i18n"

version := "0.5"

scalaVersion := "2.11.7"

val repository = "eFarmer repository" at "http://dev2.efarmer.mobi:8889/repository/internal"

resolvers += repository

publishTo := Some(repository)

credentials += Credentials(Path.userHome / ".sbt" / ".archivaCredentials")

libraryDependencies ++= {
  Seq(
    "org.scala-lang.modules" %% "scala-xml" % "1.0.5",
    "com.maximchuk.rest" % "api-client" % "0.14",

    "org.scalatest" %% "scalatest" % "2.2.4" % "test"
  )
}
    
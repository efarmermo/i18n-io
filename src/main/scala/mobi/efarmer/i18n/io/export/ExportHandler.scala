package mobi.efarmer.i18n.io.export

import java.io.{File, FileOutputStream, PrintWriter}
import java.util.{Locale, Properties, ResourceBundle}

import mobi.efarmer.i18n.io.exception.UnsupportedLocaleException

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
 * @author Maxim Maximchuk
 *         date 26-Oct-15.
 */
trait ExportHandler[T <: Any] {

  def process(localeMap: mutable.Map[String, mutable.Buffer[LocalizationEntry]]): T

}

class PropertiesExportHandler(baseFilename: String) extends ExportHandler[Unit] {

  override def process(localeMap: mutable.Map[String, mutable.Buffer[LocalizationEntry]]): Unit = {
    //writing properties
    localeMap foreach {
      case (lang, localizations) =>
        val props = new Properties()
        localizations.foreach(entry => props.put(entry.key, entry.text))
        var os = None: Option[FileOutputStream]
        try {
          os = Some(new FileOutputStream(baseFilename + s"_$lang.properties"))
          props.store(os.get, "")
        } finally {
          if (os.isDefined) os.get.close()
        }
    }
  }

}

class ResourceBundleExportHandler(locale: Locale) extends ExportHandler[ResourceBundle] {

  override def process(localeMap: mutable.Map[String, mutable.Buffer[LocalizationEntry]]): ResourceBundle = {
    localeMap.get(locale.getLanguage) match {
      case Some(localeBuffer) => new RemoteResourceBundle(locale, localeBuffer.toSeq)
      case None => throw new UnsupportedLocaleException(locale)
    }
  }

}

class CSVHandler(filename: String, splitter: String = ",") extends ExportHandler[Unit] {

  val KEY_COLUMN = "key"

  override def process(localeMap: mutable.Map[String, mutable.Buffer[LocalizationEntry]]): Unit = {

    //create header
    val header = new ArrayBuffer[String]()
    header += KEY_COLUMN
    localeMap.keySet.foreach(key => if (key == "en") header.insert(1, key) else header += key)

    //fill data
    val translateMap = new mutable.HashMap[String, mutable.HashMap[String, String]]
    localeMap foreach {
      case(lang, localizations) =>
        localizations foreach {
          case l =>
            val translates = translateMap.getOrElse(l.key, {
              val t = new mutable.HashMap[String, String]()
              translateMap += (l.key -> t)
              t
            })
            translates += (lang -> l.text)
        }
    }

    // write file
    val writer = new PrintWriter(new File(filename))
    try {
      writer.println("sep=,")
      writer.println(header.mkString(splitter))
      translateMap foreach {
        case (key, translates) =>
          val lineBuf = new ArrayBuffer[String]()
          lineBuf += escape(key, splitter)
          header.filter(_ != KEY_COLUMN).foreach(lang => lineBuf += escape(translates.getOrElse(lang, ""), splitter))
          writer.println(lineBuf.mkString(","))
      }
    } finally {
      writer.close()
    }
  }

  def escape(str: String, splitter: String): String = {
    if (str.contains(splitter)) '"' + str + '"' else str
  }

}
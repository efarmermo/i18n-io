package mobi.efarmer.i18n.io.export

import java.util
import java.util.{Locale, ResourceBundle}

import scala.collection.{JavaConversions, mutable}

/**
 * @author Maxim Maximchuk
 *         date 28.08.2015.
 */
class RemoteResourceBundle(private val locale: Locale, private val locales: Seq[LocalizationEntry]) extends ResourceBundle {

  private val lcMap = new mutable.HashMap[String, String]()
  locales.foreach(l => lcMap += (l.key -> l.text))

  override def getKeys: util.Enumeration[String] = {
    JavaConversions.asJavaEnumeration(lcMap.keysIterator)
  }

  override def handleGetObject(key: String): AnyRef = {
    lcMap.get(key) match {
      case Some(value) => value
      case None => null
    }
  }

  override def getLocale: Locale = locale
}

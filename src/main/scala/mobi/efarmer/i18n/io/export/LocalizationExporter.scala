package mobi.efarmer.i18n.io.export

import java.util.{Locale, ResourceBundle}

import com.maximchuk.rest.api.client.core.{DefaultClient, RestApiMethod}
import com.maximchuk.rest.api.client.core.RestApiMethod.Type

import scala.collection.mutable
import scala.io.Source
import scala.xml.Node
import scala.xml.pull._

/**
 * @author Maxim Maximchuk
 *         date 14.08.2015.
 */
object LocalizationExporter {
  
  var localeMapOpt = None: Option[mutable.Map[String, mutable.Buffer[LocalizationEntry]]]
  var tagOpt = None: Option[String]
  
  def connect(url: String, tag: String, ver: String = "") = {
    val method = new RestApiMethod(s"i18n/xml/$tag", Type.GET)
    if (!ver.isEmpty) method.putParam("ver", ver)
    val client = new DefaultClient(url, "export")
    val xmlString = client.execute(method).getString
    val xmlSource = Source.fromString(xmlString)
    localeMapOpt = Some(readXml(xmlSource))
    tagOpt = Some(tag)
    this
  }
  
  def handleProperties(filename: String): Unit = {
    handle(new PropertiesExportHandler(filename))
  }

  def handleResourceBundle(locale: Locale): ResourceBundle = {
    handle(new ResourceBundleExportHandler(locale))
  }

  def handleCsv(filename: String = tagOpt match {
    case Some(tag) => tag + ".csv"
    case None => throw notConnectedException
  }): Unit = {
    handle(new CSVHandler(filename))
  }

  def handle[T <: Any](handler: ExportHandler[T]): T = {
    localeMapOpt match {
      case Some(localeMap) => handler.process(localeMap)
      case None => throw notConnectedException
    }
  }

  private def readXml(xmlSource: Source): mutable.Map[String, mutable.Buffer[LocalizationEntry]] = {
    val reader = new XMLEventReader(xmlSource)
    val lcMap = new mutable.HashMap[String, mutable.Buffer[LocalizationEntry]]
    var keyOpt = None: Option[Seq[Node]]
    var langOpt = None: Option[Seq[Node]]
    reader foreach {
      case EvElemStart(_, "resource", key, _) =>
        keyOpt = key.get("key")
      case EvElemEnd(_, "resource") =>
        keyOpt = None
      case EvElemStart(_, "translate", lang, _) =>
        langOpt = lang.get("lang")
      case EvElemEnd(_, "translate") =>
        langOpt = None
      case EvText(text) =>
        (keyOpt, langOpt) match {
          case (Some(key), Some(lang)) =>
            val entry = LocalizationEntry(key.text, text)
            lcMap.get(lang.text) match {
              case Some(entries) =>
                entries += entry
              case None =>
                lcMap += (lang.text -> mutable.Buffer[LocalizationEntry](entry))
            }
          case _ => //ignore
        }
      case _ => //ignore
    }
    lcMap
  }

  def notConnectedException: IllegalAccessException = {
    new IllegalAccessException("call connect method first")
  }

}

case class LocalizationEntry(var key: String, text: String)

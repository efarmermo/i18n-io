package mobi.efarmer.i18n.io.csv

/**
  * Created by Maxim Maximchuk
  * date 25-Dec-15.
  */
object CsvStringOps {

  implicit class StringSplitter(s: String) {

    implicit def csvSplit(splitter: Char): Array[String] = {
      var parts = Array[String]()
      var part = ""
      var splitFlag = true

      for (i <- s.toCharArray.indices) {
        s.charAt(i) match {
          case c if c == '"' && (i + 1 >= s.length() || s.charAt(i + 1) != '"') =>
            splitFlag = !splitFlag
          case c if c == splitter && splitFlag =>
            parts :+= part.trim
            part = ""
          case c =>
            part += c
        }
      }

      parts :+ part.trim
    }

  }

}

package mobi.efarmer.i18n.io.exception

import java.util.Locale

/**
 * @author Maxim Maximchuk
 *         date 28.08.2015.
 */
class UnsupportedLocaleException(val locale: Locale) extends Exception(locale.getLanguage)

package mobi.efarmer.i18n.io.exception

/**
  * Created by Maxim Maximchuk
  * date 24-Dec-15.
  */
class InvalidFileFormatException(val msg: String = "invalid file format") extends RuntimeException(msg)

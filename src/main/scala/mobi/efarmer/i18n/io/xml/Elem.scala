package mobi.efarmer.i18n.io.xml

import scala.xml._

/**
  * Created by Maxim Maximchuk
  * date 24-Dec-15.
  */
case class LocalizationElem(val name: String, val resourceElem: Seq[ResourceElem])
  extends Elem(null, "localization", new UnprefixedAttribute("name", name, Null), TopScope, true, resourceElem: _*)

case class ResourceElem(val key: String, val translateElem: Seq[TranslateElem])
  extends Elem(null, "resource", new UnprefixedAttribute("key", key, Null), TopScope, false, translateElem: _*)

case class TranslateElem(val lang: String, val value: String)
  extends Elem(null, "translate", new UnprefixedAttribute("lang", lang, Null), TopScope, false, Text(value))

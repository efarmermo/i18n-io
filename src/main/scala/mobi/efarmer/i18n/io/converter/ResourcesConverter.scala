package mobi.efarmer.i18n.io.converter

import java.io.{File, PrintWriter}

import mobi.efarmer.i18n.io.exception.InvalidFileFormatException
import mobi.efarmer.i18n.io.export.LocalizationEntry
import mobi.efarmer.i18n.io.xml.{LocalizationElem, ResourceElem, TranslateElem}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import scala.util.control.Breaks._
import scala.xml.PrettyPrinter

/**
  * Created by Maxim Maximchuk
  * date 24-Dec-15.
  */
trait ResourcesConverter {

  /**
    * Need to implement for creating localization resources map
    *
    * @return map where key is language
    */
  def resources(): mutable.Map[String, mutable.Buffer[LocalizationEntry]]

  def writeXml(name: String): Unit = {
    val translateElemMap = new mutable.HashMap[String, mutable.Buffer[TranslateElem]]
    resources() foreach {
      case (lang, resList) =>
        resList.foreach(l => {
          val translateElements = translateElemMap.get(l.key) match {
            case Some(elements) => elements
            case None =>
              val buffer = new ArrayBuffer[TranslateElem]()
              translateElemMap += (l.key -> buffer)
              buffer
          }
          translateElements += TranslateElem(lang, l.text)
        })
    }

    val resourceElements = new ArrayBuffer[ResourceElem]()
    translateElemMap.foreach {
      case (key, translateElements) => resourceElements += ResourceElem(key, translateElements)
    }

    val writer = new PrintWriter(new File(s"$name.xml"))
    try {
      writer.println(new PrettyPrinter(80, 4).format(LocalizationElem(name, resourceElements)))
    } finally {
      writer.close()
    }
  }

}

class CsvConverter(val filename: String) extends ResourcesConverter {

  import mobi.efarmer.i18n.io.csv.CsvStringOps._

  var splitter = ','

  var csv = Source.fromFile(filename).getLines()

  override def resources(): mutable.Map[String, mutable.Buffer[LocalizationEntry]] = {
    val sepKeyword = "sep="

    val resources = new mutable.HashMap[String, mutable.Buffer[LocalizationEntry]]()

    val title = (csv.take(1).next() match {
      case (str) =>
        if (str.contains(sepKeyword)) {
          splitter = str.charAt(str.indexOf(sepKeyword) + sepKeyword.length)
          csv.next()
        } else str
    }).split(splitter).map(_.trim)

    var keyInd = -1
    breakable {
      for (i <- title.indices; if title(i) == "key") {
        keyInd = i
        break
      }
    }

    if (keyInd == -1) {
      throw new InvalidFileFormatException("Key column not found")
    }

    csv foreach (line => {
      val cols = line.csvSplit(splitter)
      val key = cols(keyInd)
      if (cols.length > title.length) {
        throw new InvalidFileFormatException(s"invalid column count at line: $line")
      }
      for (i <- cols.indices; if i != keyInd) {
        val lang = title(i)
        val locBuffer = resources.get(lang) match {
          case Some(buf) => buf
          case None =>
            val buf = new ArrayBuffer[LocalizationEntry]()
            resources += (lang -> buf)
            buf
        }
        locBuffer += LocalizationEntry(key, cols(i))
      }
    })
    resources
  }
}

package mobi.efarmer.i18n.io.converter

import org.scalatest.FunSuite

/**
  * Created by Maxim Maximchuk
  * date 24-Dec-15.
  */
class ResourcesConverterTest extends FunSuite {

  test("test CsvConverter") {
    new CsvConverter("web_fr.csv").writeXml("efarmer-web")
    new CsvConverter("android_fr.csv").writeXml("efarmmanager")
  }

}

package mobi.efarmer.i18n.io.export

import java.util.Locale

import org.scalatest.FunSuite

/**
 * @author Maxim Maximchuk
 *         date 14.08.2015.
 */
class LocalizationExporter$Test extends FunSuite {

  def exporter = LocalizationExporter.connect("http://efarmer.mobi:9080/localization-service/api", "efarmmanager")

  test("test connect") {
    try {
      assert(exporter != null)
    } catch {
      case e: Any => fail(e)
    }
  }

  test("test properties process") {
    try {
      exporter.handleProperties("out/locale")
    } catch {
      case e: Any => fail(e)
    }
  }

  test("test resource bundle process") {
    try {
      val locale = new Locale("en")
      val bundle = exporter.handleResourceBundle(locale)
      assert(locale == bundle.getLocale)
      assert(bundle.getString("report.note") == "Note")
    } catch  {
      case e: Any => fail(e)
    }
  }

  test("test csv process") {
    try {
      exporter.handleCsv()
    } catch {
      case e: Any => fail(e)
    }
  }

}
